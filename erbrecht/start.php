<?php
include('structure/head.php');
include('structure/nav.php')
?>

<div class="container">
	<div class="row">
		<div class="valign-wrapper" style="height: 80%">
			<div class="input-field col s6 offset-s3">
				<i class="material-icons prefix grey-text">search</i>
				<input placeholder="Ihre Rechtsfrage..." id="search" type="text">
				<div class="row">
					<br>
					<div class="col s4 offset-s4"><a class="waves-effect waves-light btn btn-block" href="workflow">Jetzt suchen</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
include('structure/login_modal.php'); 
include('structure/footer.php');
?>