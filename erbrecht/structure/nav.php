<nav class="blue darken-3" role="navigation">
	<div class="nav-wrapper container"><a id="logo-container" class="brand-logo" href="start">JURI</a>
		<ul class="right hide-on-med-and-down">
			<li><a class="dropdown-trigger" href="#" data-target="dropdown1"><i class="material-icons">menu</i></a></li>

			<ul id='dropdown1' class='dropdown-content' style="width: 300px !important">
				<li><a class="modal-trigger" href="#modal1">Login/Registrierung</a></li>
				<li class="divider" tabindex="-1"></li>
				<li><a href="#!">Mein Stammbaum</a></li>
			</ul>
		</ul>
	</div>
</nav>
