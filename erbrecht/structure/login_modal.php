<div id="modal1" class="modal">
	<div class="modal-content">
		<div class="row">
			<div class="col s8 offset-s2">
				<h4 class="center">Anmeldung</h4>
			</div>
			<div class="col s1 offset-s1">
				 <a class="waves-effect modal-close waves-teal btn-flat" href="#!"><i class="material-icons center">close</i></a>
			</div>
		</div>
		<div class="row">
			<div class="col s6">
				<h5>Login</h5>
				<div class="input-field col s12">
					<input placeholder="E-Mail" id="userLogin" type="text">
					<label for="userLogin">E-Mail</label>
				</div>
				<div class="input-field col s12">
					<input placeholder="Passwort" id="userPw" type="password">
					<label for="userPw">Passwort</label>
				</div>
				<div class="col s6 offset-s3"><a class="waves-effect waves-light btn btn-block">Einloggen</a></div>
			</div>
			<div class="col s6" style="border-left: 1px solid grey">
				<h5>Registrierung</h5>
				<div class="input-field col s12">
					<input placeholder="Name" id="regName" type="text">
					<label for="regName">Name</label>
				</div>
				<div class="input-field col s12">
					<input placeholder="E-Mail" id="regMail" type="text">
					<label for="regMail">E-Mail</label>
				</div>
				<div class="input-field col s12">
					<input placeholder="Passwort" id="regPw" type="password">
					<label for="regPw">Passwort</label>
				</div>
				<div class="input-field col s12">
					<input placeholder="Passwort wiederholen" id="regPw2" type="password">
					<label for="regPw2">Passwort wiederholen</label>
				</div>
				<div class="col s6 offset-s3"><a class="waves-effect waves-light btn btn-block">Registrieren</a></div>
			</div>
		</div>
	</div>
</div>