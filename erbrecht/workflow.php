<?php
include('structure/head.php');
include('structure/nav.php')
?>

<div class="container">
	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s2 offset-s1" id="testamentTab"><a class="active" href="#testament"><span>Testament</span><i class="inline-icon material-icons">arrow_forward</i></a></li>
				<li class="tab col s2 disabled" id="partnerTab"><a class="" href="#partner"><span>Partner</span><i class="inline-icon material-icons">arrow_forward</i></a></li>
				<li class="tab col s2 disabled" id="childrenTab"><a class="" href="#children"><span>Kinder</span><i class="inline-icon material-icons">arrow_forward</i></a></li>
				<li class="tab col s2 disabled" id="parentsTab"><a class="" href="#parents"><span>Eltern</span><i class="inline-icon material-icons">arrow_forward</i></a></li>
				<li class="tab col s2 disabled" id="resultsTab"><a class="" href="#results">Ergebnis</a></li>
			</ul>
			<!--TESTAMENT-->
			<div class="col s12 center" id="testament">
				<h3 class="teal-text">Liegt ein Testament vor?</h3>
				<br>
				<p>
					<span style="margin-right: 50px">
						<label>
							<input class="with-gap" type="radio" name="tesRadio"/>
							<span style="font-size: 20px">Ja</span>
						</label>
					</span>
					<span>
						<label>
							<input class="with-gap" type="radio" name="tesRadio"/>
							<span style="font-size: 20px">Nein</span>
						</label>
					</span>
				</p>
				<br>
				<div class="col s2 offset-s5"><a class="waves-effect waves-light btn btn-block" href="#partner" id="toPartner">Weiter</a></div>
			</div>
			<!--PARTNER-->
			<div class="col s12 center" id="partner">
				<h3 class="teal-text">Leben Sie in einer Ehe?</h3>
				<br>
				<p>
					<span style="margin-right: 50px">
						<label>
							<input class="with-gap" type="radio" name="partRadio" id="hasPartner"/>
							<span style="font-size: 20px">Ja</span>
						</label>
					</span>
					<span>
						<label>
							<input class="with-gap" type="radio" name="partRadio"/>
							<span style="font-size: 20px">Nein</span>
						</label>
					</span>
				</p>
				<div style="display: none; margin-bottom: 25px;" class="left-align col offset-s4" id="partKindDiv">
					<div style="margin-left: 74px;">
						<span>
							<label>
								<input class="with-gap" type="radio" name="partKindRadio"/>
								<span style="font-size: 20px">Gütertrennung</span>
							</label>
						</span>
						<br>
						<span>
							<label>
								<input class="with-gap" type="radio" name="partKindRadio"/>
								<span style="font-size: 20px">Zugewinngemeinschaft</span>
							</label>
						</span>
						<br>
						<span>
							<label>
								<input class="with-gap" type="radio" name="partKindRadio"/>
								<span style="font-size: 20px">Scheidungsantrag</span>
							</label>
						</span>
					</div>
				</div>
				<br>
				<div class="col s2 offset-s5"><a class="waves-effect waves-light btn btn-block" href="#children" id="toChildren">Weiter</a></div>
			</div>
			<!--CHILDREN-->
			<div class="col s12 center" id="children">
				<h3 class="teal-text">Anzahl Ihrer <a class="tooltipped" data-position="bottom" data-tooltip="Da Erbanteile nur an lebende Personen gegeben werden können, ist hier nur die Anzahl der lebenden Kinder relevant.">lebenden</a> Kinder?</h3>
				<br>
				<div class="col s4 offset-s4" style="margin-bottom: 24px">
					<input placeholder="Anzahl lebender Kinder" id="childNr" type="number" min="0">
				</div>
				<br>
				<div class="col s2 offset-s5"><a class="waves-effect waves-light btn btn-block" href="#parents" id="toParents">Weiter</a></div>
			</div>
			<!--PARENTS-->
			<div class="col s12 center" id="parents">
				<h3 class="teal-text">Anzahl Ihrer <a class="tooltipped" data-position="bottom" data-tooltip="Da Erbanteile nur an lebende Personen gegeben werden können, ist hier nur die Anzahl der lebenden Elternteile relevant.">lebenden</a> Elternteile?</h3>
				<br>
				<div class="col s4 offset-s4" style="margin-bottom: 24px">
					<input placeholder="Anzahl lebender Elternteile" id="parentNr" type="number" min="0" max="2">
				</div>
				<br>
				<div class="col s2 offset-s5"><a class="waves-effect waves-light btn btn-block" href="#results" id="toResults">Weiter</a></div>
			</div>
			<!--RESULTS-->
			<div class="col s12 center" id="results">
				<h3 class="teal-text">Ergebnis</h3>
				<canvas id="resultChart" width="450" height="450"></canvas>
				<div class="row">
					<br>
					<div class="col s2 offset-s5"><a class="waves-effect waves-light btn btn-block modal-trigger" href="#modal1">Speichern</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/Chart.min.js"></script>
<?php 
include('structure/login_modal.php'); 
include('structure/footer.php');
?>