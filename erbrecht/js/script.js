$(document).ready(function(){
	$('.modal').modal();
	$('ul.tabs').tabs();
	$('.tooltipped').tooltip();
	$('.dropdown-trigger').dropdown({alignment: 'right', constrainWidth: false });
	$("#toPartner").click(function() {
		$('#partnerTab').removeClass('disabled');
		$('ul.tabs').tabs('select', 'partner');
	});
	$("#toChildren").click(function() {
		$('#childrenTab').removeClass('disabled');
		$('ul.tabs').tabs('select', 'children');
	});
	$("#toParents").click(function() {
		$('#parentsTab').removeClass('disabled');
		$('ul.tabs').tabs('select', 'parents');
	});
	$("#toResults").click(function() {
		$('#resultsTab').removeClass('disabled');
		$('ul.tabs').tabs('select', 'results');
	});


	$('input:radio[name="partRadio"]').change(
		function(){
			if($('#hasPartner').is(':checked')) {
				$('#partKindDiv').show();
			} else {
				$('#partKindDiv').hide();
			}
		});

	var ctx = document.getElementById('resultChart').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: ['Partner', 'Kind 1', 'Kind 2', 'Kind 3'],
			datasets: [{
				label: '',
				data: [1, 1, 1, 1],
				backgroundColor: [
				'#1e88e5',
				'#00897b',
				'#cddc39',
				'#fb8c00',
				],
			}]
		},
		options: {
                responsive: false
            }
	});
});